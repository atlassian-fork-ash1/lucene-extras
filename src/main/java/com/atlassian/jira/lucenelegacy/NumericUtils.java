package com.atlassian.jira.lucenelegacy;

/**
 * Port of methods from Lucene 3.3.0 that were removed from {@link org.apache.lucene.util.NumericUtils}.
 */
public class NumericUtils {

    /**
     * Expert: Longs are stored at lower precision by shifting off lower bits. The shift count is
     * stored as <code>SHIFT_START_LONG+shift</code> in the first character
     */
    public static final char SHIFT_START_LONG = (char) 0x20;

    /**
     * Expert: The maximum term length (used for <code>char[]</code> buffer size)
     * for encoding <code>long</code> values.
     *
     * @see #longToPrefixCoded(long, int, char[])
     */
    public static final int BUF_SIZE_LONG = 63 / 7 + 2;

    /**
     * Expert: Returns prefix coded bits after reducing the precision by <code>shift</code> bits.
     * This is method is used by <code>NumericTokenStream</code>.
     *
     * @param val    the numeric value
     * @param shift  how many bits to strip from the right
     * @param buffer that will contain the encoded chars, must be at least of {@link #BUF_SIZE_LONG}
     *               length
     * @return number of chars written to buffer
     */
    public static int longToPrefixCoded(final long val, final int shift, final char[] buffer) {
        if (shift > 63 || shift < 0)
            throw new IllegalArgumentException("Illegal shift value, must be 0..63");
        int nChars = (63 - shift) / 7 + 1, len = nChars + 1;
        buffer[0] = (char) (SHIFT_START_LONG + shift);
        long sortableBits = val ^ 0x8000000000000000L;
        sortableBits >>>= shift;
        while (nChars >= 1) {
            // Store 7 bits per character for good efficiency when UTF-8 encoding.
            // The whole number is right-justified so that lucene can prefix-encode
            // the terms more efficiently.
            buffer[nChars--] = (char) (sortableBits & 0x7f);
            sortableBits >>>= 7;
        }
        return len;
    }

    /**
     * Expert: Returns prefix coded bits after reducing the precision by <code>shift</code> bits.
     * This is method is used by <code>LongRangeBuilder</code>.
     *
     * @param val   the numeric value
     * @param shift how many bits to strip from the right
     */
    public static String longToPrefixCoded(final long val, final int shift) {
        final char[] buffer = new char[BUF_SIZE_LONG];
        final int len = longToPrefixCoded(val, shift, buffer);
        return new String(buffer, 0, len);
    }

    /**
     * This is a convenience method, that returns prefix coded bits of a long without
     * reducing the precision. It can be used to store the full precision value as a
     * stored field in index.
     * <p>To decode, use {@link #prefixCodedToLong}.
     */
    public static String longToPrefixCoded(final long val) {
        return longToPrefixCoded(val, 0);
    }

    /**
     * Returns a long from prefixCoded characters.
     * Rightmost bits will be zero for lower precision codes.
     * This method can be used to decode e.g. a stored field.
     *
     * @throws NumberFormatException if the supplied string is
     *                               not correctly prefix encoded.
     * @see #longToPrefixCoded(long)
     */
    public static long prefixCodedToLong(final String prefixCoded) {
        final int shift = prefixCoded.charAt(0) - SHIFT_START_LONG;
        if (shift > 63 || shift < 0)
            throw new NumberFormatException("Invalid shift value in prefixCoded string (is encoded value really a LONG?)");
        long sortableBits = 0L;
        for (int i = 1, len = prefixCoded.length(); i < len; i++) {
            sortableBits <<= 7;
            final char ch = prefixCoded.charAt(i);
            if (ch > 0x7f) {
                throw new NumberFormatException(
                        "Invalid prefixCoded numerical value representation (char " +
                                Integer.toHexString(ch) + " at position " + i + " is invalid)"
                );
            }
            sortableBits |= ch;
        }
        return (sortableBits << shift) ^ 0x8000000000000000L;
    }

    /**
     * Convenience method: this just returns:
     * longToPrefixCoded(doubleToSortableLong(val))
     */
    public static String doubleToPrefixCoded(double val) {
        return longToPrefixCoded(org.apache.lucene.util.NumericUtils.doubleToSortableLong(val));
    }
}
