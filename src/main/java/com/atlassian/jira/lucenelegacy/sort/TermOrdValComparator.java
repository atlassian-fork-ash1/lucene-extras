package com.atlassian.jira.lucenelegacy.sort;

import org.apache.lucene.index.DocValues;
import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.index.SortedDocValues;
import org.apache.lucene.search.FieldComparator;
import org.apache.lucene.search.LeafFieldComparator;
import org.apache.lucene.search.Scorer;
import org.apache.lucene.util.BytesRef;
import org.apache.lucene.util.BytesRefBuilder;

import java.io.IOException;
import java.util.Arrays;

/**
 * Based on {@link FieldComparator.TermOrdValComparator}, this version is modified to not eagerly allocate
 * very large arrays.
 */
public final class TermOrdValComparator extends FieldComparator<BytesRef> implements LeafFieldComparator {
    /* Ords for each slot.
       @lucene.internal */
    private int[] ords;

    /* Values for each slot.
       @lucene.internal */
    private BytesRef[] values;
    private BytesRefBuilder[] tempBRs;

    /* Which reader last copied a value into the slot. When
       we compare two slots, we just compare-by-ord if the
       readerGen is the same; else we must compare the
       values (slower).
       @lucene.internal */
    private int[] readerGen;

    /* Gen of current reader we are on.
       @lucene.internal */
    int currentReaderGen = -1;

    /* Current reader's doc ord/values.
       @lucene.internal */
    SortedDocValues termsIndex;

    private final String field;

    /* Bottom slot, or -1 if queue isn't full yet
       @lucene.internal */
    int bottomSlot = -1;

    /* Bottom ord (same as ords[bottomSlot] once bottomSlot
       is set).  Cached for faster compares.
       @lucene.internal */
    int bottomOrd;

    /* True if current bottom slot matches the current
       reader.
       @lucene.internal */
    boolean bottomSameReader;

    /* Bottom value (same as values[bottomSlot] once
       bottomSlot is set).  Cached for faster compares.
      @lucene.internal */
    BytesRef bottomValue;

    /** Set by setTopValue. */
    BytesRef topValue;
    boolean topSameReader;
    int topOrd;

    /** -1 if missing values are sorted first, 1 if they are
     *  sorted last */
    final int missingSortCmp;

    /** Which ordinal to use for a missing value. */
    final int missingOrd;

    /** Creates this, sorting missing values first. */
    public TermOrdValComparator(int numHits, String field) {
        this(numHits, field, false);
    }

    private final int numHits;

    /** Creates this, with control over how missing values
     *  are sorted.  Pass sortMissingLast=true to put
     *  missing values at the end. */
    public TermOrdValComparator(int numHits, String field, boolean sortMissingLast) {
        //atlassian-only modification: only allocate small arrays to start off with
        //------------------------->
        this.numHits = numHits;
        final int initialSize = Math.min(1024, numHits);
        ords = new int[initialSize];
        values = new BytesRef[initialSize];
        tempBRs = new BytesRefBuilder[initialSize];
        readerGen = new int[initialSize];
        //<------------------

        this.field = field;
        if (sortMissingLast) {
            missingSortCmp = 1;
            missingOrd = Integer.MAX_VALUE;
        } else {
            missingSortCmp = -1;
            missingOrd = -1;
        }
    }

    private int getOrdForDoc(int doc) throws IOException {
        if (termsIndex.advanceExact(doc)) {
            return termsIndex.ordValue();
        } else {
            return -1;
        }
    }

    @Override
    public int compare(int slot1, int slot2) {
        if (readerGen[slot1] == readerGen[slot2]) {
            return ords[slot1] - ords[slot2];
        }

        final BytesRef val1 = values[slot1];
        final BytesRef val2 = values[slot2];
        if (val1 == null) {
            if (val2 == null) {
                return 0;
            }
            return missingSortCmp;
        } else if (val2 == null) {
            return -missingSortCmp;
        }
        return val1.compareTo(val2);
    }

    @Override
    public int compareBottom(int doc) throws IOException {
        assert bottomSlot != -1;
        int docOrd = getOrdForDoc(doc);
        if (docOrd == -1) {
            docOrd = missingOrd;
        }
        if (bottomSameReader) {
            // ord is precisely comparable, even in the equal case
            return bottomOrd - docOrd;
        } else if (bottomOrd >= docOrd) {
            // the equals case always means bottom is > doc
            // (because we set bottomOrd to the lower bound in
            // setBottom):
            return 1;
        } else {
            return -1;
        }
    }


    /**
     * Atlassian patch to dynamically increase the size of the arrays here as we need them.
     *
     * @param slot Slot to make sure we have capacity to store. If the internal arrays aren't at least this big, we need to resize.
     */
    private void ensureCapacity(int slot) {
        if (values.length <= slot) {
            final int newSize = Math.min(numHits, slot * 2);

            values = Arrays.copyOf(values, newSize);
            tempBRs = Arrays.copyOf(tempBRs, newSize);
            ords = Arrays.copyOf(ords, newSize);
            readerGen = Arrays.copyOf(readerGen, newSize);
        }
    }

    @Override
    public void copy(int slot, int doc) throws IOException {

        ensureCapacity(slot);

        int ord = getOrdForDoc(doc);
        if (ord == -1) {
            ord = missingOrd;
            values[slot] = null;
        } else {
            assert ord >= 0;
            if (tempBRs[slot] == null) {
                tempBRs[slot] = new BytesRefBuilder();
            }
            tempBRs[slot].copyBytes(termsIndex.lookupOrd(ord));
            values[slot] = tempBRs[slot].get();
        }
        ords[slot] = ord;
        readerGen[slot] = currentReaderGen;
    }

    /** Retrieves the SortedDocValues for the field in this segment */
    protected SortedDocValues getSortedDocValues(LeafReaderContext context, String field) throws IOException {
        return DocValues.getSorted(context.reader(), field);
    }

    @Override
    public LeafFieldComparator getLeafComparator(LeafReaderContext context) throws IOException {
        termsIndex = getSortedDocValues(context, field);
        currentReaderGen++;

        if (topValue != null) {
            // Recompute topOrd/SameReader
            int ord = termsIndex.lookupTerm(topValue);
            if (ord >= 0) {
                topSameReader = true;
                topOrd = ord;
            } else {
                topSameReader = false;
                topOrd = -ord-2;
            }
        } else {
            topOrd = missingOrd;
            topSameReader = true;
        }

        if (bottomSlot != -1) {
            // Recompute bottomOrd/SameReader
            setBottom(bottomSlot);
        }

        return this;
    }

    @Override
    public void setBottom(final int bottom) throws IOException {
        bottomSlot = bottom;

        bottomValue = values[bottomSlot];
        if (currentReaderGen == readerGen[bottomSlot]) {
            bottomOrd = ords[bottomSlot];
            bottomSameReader = true;
        } else {
            if (bottomValue == null) {
                // missingOrd is null for all segments
                assert ords[bottomSlot] == missingOrd;
                bottomOrd = missingOrd;
                bottomSameReader = true;
                readerGen[bottomSlot] = currentReaderGen;
            } else {
                final int ord = termsIndex.lookupTerm(bottomValue);
                if (ord < 0) {
                    bottomOrd = -ord - 2;
                    bottomSameReader = false;
                } else {
                    bottomOrd = ord;
                    // exact value match
                    bottomSameReader = true;
                    readerGen[bottomSlot] = currentReaderGen;
                    ords[bottomSlot] = bottomOrd;
                }
            }
        }
    }

    @Override
    public void setTopValue(BytesRef value) {
        // null is fine: it means the last doc of the prior
        // search was missing this value
        topValue = value;
    }

    @Override
    public BytesRef value(int slot) {
        return values[slot];
    }

    @Override
    public int compareTop(int doc) throws IOException {

        int ord = getOrdForDoc(doc);
        if (ord == -1) {
            ord = missingOrd;
        }

        if (topSameReader) {
            // ord is precisely comparable, even in the equal
            // case
            return topOrd - ord;
        } else if (ord <= topOrd) {
            // the equals case always means doc is < value
            // (because we set lastOrd to the lower bound)
            return 1;
        } else {
            return -1;
        }
    }

    @Override
    public int compareValues(BytesRef val1, BytesRef val2) {
        if (val1 == null) {
            if (val2 == null) {
                return 0;
            }
            return missingSortCmp;
        } else if (val2 == null) {
            return -missingSortCmp;
        }
        return val1.compareTo(val2);
    }

    @Override
    public void setScorer(Scorer scorer) {}
}

