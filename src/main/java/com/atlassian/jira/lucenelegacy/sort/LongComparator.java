package com.atlassian.jira.lucenelegacy.sort;

import org.apache.lucene.search.FieldComparator;

import java.io.IOException;
import java.util.Arrays;

/**
 * Based on {@link FieldComparator.LongComparator}, this version is modified to not eagerly allocate
 * very large arrays.
 * <br>
 * Parses field's values as long (using {@link
 *  org.apache.lucene.index.LeafReader#getNumericDocValues(String)} and sorts by ascending value
 */
public class LongComparator extends FieldComparator.NumericComparator<Long> {
    private long[] values;
    private long bottom;
    private long topValue;
    private final int numHits;

    /**
     * Creates a new comparator based on {@link Long#compare} for {@code numHits}.
     * When a document has no value for the field, {@code missingValue} is substituted.
     */
    public LongComparator(int numHits, String field, Long missingValue) {
        super(field, missingValue != null ? missingValue : 0L);
        this.numHits = numHits;
        values = new long[numHits];
    }

    private long getValueForDoc(int doc) throws IOException {
        if (currentReaderValues.advanceExact(doc)) {
            return currentReaderValues.longValue();
        } else {
            return missingValue;
        }
    }

    @Override
    public int compare(int slot1, int slot2) {
        return Long.compare(values[slot1], values[slot2]);
    }

    @Override
    public int compareBottom(int doc) throws IOException {
        return Long.compare(bottom, getValueForDoc(doc));
    }

    /**
     * Atlassian patch to dynamically increase the size of the arrays here as we need them.
     *
     * @param slot Slot to make sure we have capacity to store. If the values array isn't bigger than this, we need to resize
     */
    private void ensureCapacity(int slot) {
        if (values.length <= slot) {
            final int newSize = Math.min(numHits, slot * 2);

            values = Arrays.copyOf(values, newSize);
        }

    }
    @Override
    public void copy(int slot, int doc) throws IOException {
        ensureCapacity(slot);

        values[slot] = getValueForDoc(doc);
    }

    @Override
    public void setBottom(final int bottom) {
        this.bottom = values[bottom];
    }

    @Override
    public void setTopValue(Long value) {
        topValue = value;
    }

    @Override
    public Long value(int slot) {
        return Long.valueOf(values[slot]);
    }

    @Override
    public int compareTop(int doc) throws IOException {
        return Long.compare(topValue, getValueForDoc(doc));
    }
}
